import format from 'date-fns/format';
import { ru } from 'date-fns/locale';

export const formatDay = (date) => format(date, 'EEEE', { locale: ru });

export const formatDate = (date) => format(date, 'd MMMM', { locale: ru });
