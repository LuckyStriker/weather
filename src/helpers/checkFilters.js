const checkType = (type, element) => type === element;

const checkMinTemperature
    = (temperature, elementTemperature) => elementTemperature > temperature;

const checkMaxTemperature
    = (temperature, elementTemperature) => elementTemperature < temperature;

export const checkFilters = (filters, filterKeys, forecastItem) => filterKeys.every((key) => {
    if (key === 'sunny' || key === 'cloudy') {
        return checkType(key, forecastItem.type);
    }
    if (key === 'minTemperature') {
        return checkMinTemperature(Number(filters[ key ]), forecastItem.temperature);
    }
    if (key === 'maxTemperature') {
        return checkMaxTemperature(Number(filters[ key ]), forecastItem.temperature);
    }

    return false;
});
