import { render } from 'react-dom';
import { QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';
import { App } from './App';
import './theme/index.scss';
import { queryClient } from './lib/react-query';
import { store } from './lib/redux/init/store';

render(
    <Provider store = { store }>
        <QueryClientProvider client = { queryClient }>
            <App />
        </QueryClientProvider>
    </Provider>,
    document.getElementById('root'),
);
