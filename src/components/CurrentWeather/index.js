import { useSelector } from 'react-redux';
import {
    getCurrentForecastTemperature,
    getCurrentForecastRainProbability,
    getCurrentForecastHumidity,
} from '../../lib/redux/selectors';

export const CurrentWeather = () => {
    const temperature = useSelector(getCurrentForecastTemperature);
    const rainProbability = useSelector(getCurrentForecastRainProbability);
    const humidity = useSelector(getCurrentForecastHumidity);

    return (
        <div className = 'current-weather'>
            <p className = 'temperature'>{ temperature }</p>
            <p className = 'meta'>
                <span className = 'rainy'>{ rainProbability }</span>
                <span className = 'humidity'>{ humidity }</span>
            </p>
        </div>
    );
};
