import { useSelector } from 'react-redux';
import { formatDay } from '../../../helpers';
import { getCurrentForecastId } from '../../../lib/redux/selectors';

export const ForecastDay = (props) => {
    const {
        id, type, day, temperature, onClick,
    } = props;
    const currentId = useSelector(getCurrentForecastId);
    const formattedDay = formatDay(day);
    const selectClass = currentId === id ? 'selected' : '';

    const handleClick = () => {
        onClick(props);
    };

    return (
        <div
            className = { `day ${type} ${selectClass}` }
            role = 'button'
            tabIndex = '0'
            onKeyPress = { handleClick }
            onClick = { handleClick }>
            <p>{ formattedDay }</p>
            <span>{ temperature }</span>
        </div>
    );
};
