import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { ForecastDay } from './ForecastDay';
import { forecastActions } from '../../lib/redux/actions';

export const Forecast = ({ forecast }) => {
    const dispatch = useDispatch();

    const handleForecastDayClick = useCallback((forecastItem) => {
        dispatch(forecastActions.setCurrentForecast(forecastItem));
    }, []);

    const forecastJSX = forecast?.map((forecastItem) => (
        <ForecastDay
            key = { forecastItem.id }
            { ...forecastItem }
            onClick = { handleForecastDayClick } />
    ));

    return (
        <div className = 'forecast'>{ forecastJSX?.length > 0 ? forecastJSX : (
            <p className = 'message'>По заданным критериям нет доступных дней!</p>
        ) }</div>
    );
};
