export const Checkbox = ({
    label, value, onClick, register,
}) => {
    const selectedClass = value ? 'selected' : '';

    const handleOnClick = () => {
        onClick(register.name, !value);
    };

    return (
        <span className = { `checkbox ${selectedClass}` } onClick = { handleOnClick }>
            { label }
        </span>
    );
};
