export const Button = ({ disabled, children, onClick }) => (
    <button
        type = 'submit' disabled = { disabled }
        onClick = { onClick }>
        { children }
    </button>
);
