export const CustomInput = ({ id, label, register }) => (
    <p className = 'custom-input'>
        <label htmlFor = { id }>{ label }</label>
        <input
            id = { id }
            type = 'number'
            { ...register } />
    </p>
);
