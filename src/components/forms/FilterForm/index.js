import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Checkbox, CustomInput, Button } from '../elements';
import { filtersActions } from '../../../lib/redux/actions';

export const Filter = () => {
    const dispatch = useDispatch();
    const {
        register, handleSubmit, reset, watch, setValue, formState,
    } = useForm({
        defaultValues: {
            cloudy:         false,
            sunny:          false,
            maxTemperature: '',
            minTemperature: '',
        },
    });
    const watchIsCloudy = watch('cloudy');
    const watchIsSunny = watch('sunny');

    const filter = useCallback(handleSubmit((data) => {
        dispatch(filtersActions.setFilters(data));
    }), []);

    const handleCheckboxClick = useCallback((name, value) => {
        if (watchIsCloudy) {
            setValue('cloudy', undefined);
        }
        if (watchIsSunny) {
            setValue('sunny', undefined);
        }

        setValue(name, value, { shouldDirty: true });
    }, [watchIsCloudy, watchIsSunny]);

    const handleResetClick = () => {
        const data = {
            cloudy:         false,
            sunny:          false,
            maxTemperature: '',
            minTemperature: '',
        };

        dispatch(filtersActions.setFilters(data));

        reset();
    };

    return (
        <div className = 'filter'>
            <Checkbox
                label = 'Облачно'
                value = { watchIsCloudy }
                register = { register('cloudy') }
                onClick = { handleCheckboxClick } />
            <Checkbox
                label = 'Солнечно'
                value = { watchIsSunny }
                register = { register('sunny') }
                onClick = { handleCheckboxClick } />
            <CustomInput
                id = 'min-temperature'
                label = 'Минимальная температура'
                register = { register('minTemperature') } />
            <CustomInput
                id = 'max-temperature'
                label = 'Максимальная температура'
                register = { register('maxTemperature') } />
            { formState.isSubmitSuccessful ? (
                <Button onClick = { handleResetClick }>Сбросить</Button>
            ) : (
                <Button
                    disabled = { !formState.isDirty }
                    onClick = { filter }>Отфильтровать</Button>
            ) }
        </div>
    );
};
