import { useSelector } from 'react-redux';
import { getCurrentForecastDay, getCurrentForecastType } from '../../lib/redux/selectors';
import { formatDay, formatDate } from '../../helpers';

export const Head = () => {
    const type = useSelector(getCurrentForecastType);
    const day = useSelector(getCurrentForecastDay);

    return (
        <div className = 'head'>
            <div className = { `icon ${type}` } />
            <div className = 'current-date'>
                <p>{ day ? formatDay(day) : '' }</p>
                <span>{ day ? formatDate(day) : '' }</span>
            </div>
        </div>
    );
};
