import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import {
    Forecast, Head, CurrentWeather,
} from './components';
import { Filter } from './components/forms';
import { getForecastFilters } from './lib/redux/selectors';
import { checkFilters } from './helpers';
import { useForecast } from './hooks';
import { forecastActions } from './lib/redux/actions';

export const App = () => {
    const dispatch = useDispatch();
    const filters = useSelector(getForecastFilters);
    const { data: forecast, isFetched } = useForecast();

    useEffect(() => {
        if (forecast) {
            dispatch(forecastActions.setCurrentForecast(forecast[ 0 ]));
        }
    }, [isFetched]);

    const filterKeys = Object.keys(filters).filter((keyItem) => filters[ keyItem ]);

    const filteredForecast = filterKeys.length > 0 ? forecast?.filter(
        (forecastItem) => checkFilters(filters, filterKeys, forecastItem),
    ) : forecast;

    const slicedForecast = filteredForecast?.slice(0, 7);

    return (
        <main>
            <Filter />
            { slicedForecast?.length > 0 ? (
                <>
                    <Head />
                    <CurrentWeather />
                </>
            ) : null }
            <Forecast forecast = { slicedForecast } />
        </main>
    );
};

