import { forecastTypes } from '../types';

export const forecastActions = Object.freeze({
    setCurrentForecast: (forecast) => {
        return {
            type:    forecastTypes.SET_CURRENT_FORECAST,
            payload: forecast,
        };
    },
});
