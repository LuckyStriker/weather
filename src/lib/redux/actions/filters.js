import { filtersTypes } from '../types';

export const filtersActions = Object.freeze({
    setFilters: (filters) => {
        return {
            type:    filtersTypes.SET_FILTERS,
            payload: filters,
        };
    },
});
