import { filtersTypes } from '../types';

const initialState = {
    cloudy:         false,
    sunny:          false,
    maxTemperature: '',
    minTemperature: '',
};

export const filtersReducer = (state = initialState, action) => {
    switch (action.type) {
    case filtersTypes.SET_FILTERS: {
        return {
            ...state,
            ...action.payload,
        };
    }
    default: {
        return state;
    }
    }
};
