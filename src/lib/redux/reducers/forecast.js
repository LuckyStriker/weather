import { forecastTypes } from '../types';

const initialState = {
    id:               '',
    type:             '',
    day:              null,
    rain_probability: null,
    humidity:         null,
    temperature:      null,
};

export const forecastReducer = (state = initialState, action) => {
    switch (action.type) {
    case forecastTypes.SET_CURRENT_FORECAST: {
        return {
            ...state,
            ...action.payload,
        };
    }
    default: {
        return state;
    }
    }
};
