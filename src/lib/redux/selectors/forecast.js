export const getCurrentForecastId = (state) => state.forecast.id;

export const getCurrentForecastType = (state) => state.forecast.type;

export const getCurrentForecastDay = (state) => state.forecast.day;

export const getCurrentForecastRainProbability = (state) => state.forecast.rain_probability;

export const getCurrentForecastHumidity = (state) => state.forecast.humidity;

export const getCurrentForecastTemperature = (state) => state.forecast.temperature;

export const getForecastFilters = (state) => state.filters;
