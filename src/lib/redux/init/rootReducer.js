import { combineReducers } from 'redux';
import { forecastReducer as forecast, filtersReducer as filters } from '../reducers';

export const rootReducer = combineReducers({
    forecast,
    filters,
});
