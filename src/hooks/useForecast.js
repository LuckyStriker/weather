import { useQuery } from 'react-query';

import { api } from '../api';

export const useForecast = () => useQuery('forecast', api.getWeather);
